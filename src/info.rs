use actix_web::HttpResponse;

use crate::constants::APPLICATION_JSON;
use std::collections::HashMap;

#[get("/info")]
pub async fn info() -> HttpResponse {
    let info: HashMap<&str, &str> = HashMap::from([("name", "RustAgendaAPI"), ("version", "0.0.1")]);

    HttpResponse::Ok().content_type(APPLICATION_JSON).json(info)
}
